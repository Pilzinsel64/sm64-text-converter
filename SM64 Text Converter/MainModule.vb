﻿Imports DevComponents.DotNetBar

Module MainModule

    Sub Main()
        Application.EnableVisualStyles()
        StyleManager.Style = eStyle.Office2016
        Application.Run(New SM64_ROM_Manager.Form_SM64TextConverter)
    End Sub

End Module
